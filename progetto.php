<?php include('_partials/header.inc'); ?>

		<div class="container">

			<div class="row">

				<div class="swiper-outer span12">
					<a class="arrow-left " href="#"></a>
					<a class="arrow-right " href="#"></a>
						<div id="slider1" class="swiper-container my_slider">
							<div class="swiper-wrapper">
								<div class="swiper-slide"> <img src="img/render/fotoinserimento-piazza.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-1-ok.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-2.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-3-new.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-4-new.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-7-new.jpg"> </div>
								<div class="swiper-slide"> <img src="img/render/IMM-9-new.jpg"> </div>
							</div>
						</div>
					<div class=" my_pagination"></div>
				</div>

			</div><!--/row-->

			<div class="row">

				<div class="span12">
					<h3>Museo dell'Identità - Nuoro</h3>
					<p>RECUPERO  FUNZIONALE DELL’EX- MULINO GUISO GALLISAI DESTINATO
					AD ACCOGLIERE IL “MUSEO E LABORATORI DELL’IDENTITA</p>
					<p>L’edificio,  un  tempo  con  destinazione  d’uso  industriale,  risulta  essere  in disuso  dagli  anni  ’70. Il progetto prevede il cambio di destinazione d’uso in museo</p>
					<p>L’edificio oggetto di intervento è ubicato al bordo del centro storico, in stretta relazione con il sistema degli spazi pubblici più importanti della città di Nuoro all’interno di un tessuto storico formato prevalentemente da edifici residenziali di 2-3, confinante (in via delle Conce) con la zona B di espansione composta da edifici plurifamiliari di 6-7 piani costruiti nei primi anni ’70.</p>
					<p>La sua localizzazione all’interno del contesto urbano risulta particolarmente strategica: da un lato l’immobile ricade all’interno del centro storico a poca distanza da numerosi ed importanti servizi, pubblici e privati, scuole, uffici, negozi, oltre la casa natale di Grazia Deledda, la biblioteca Sebastiano Satta", la Cattedrale di S. Maria della Neve, la chiesa del Rosario, la sede dell'Amministrazione Provinciale e di quella Comunale e dall’altro si apre al paesaggio imponente della grande valle di “Badde Manna” che separa Nuoro da Oliena.</p>
				</div>

			</div><!--/row-->

			<hr>

			<footer>
				<p>&copy; Studio di architettura Marcon Architetti 2013</p>
			</footer>

		</div> <!-- /container -->

<?php include('_partials/footer.inc'); ?>