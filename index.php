<?php include('_partials/header.inc'); ?>

        <div class="container">

            <div class="row">

                <div class="span12">
                    <img class="img-polaroid" src="img/image001.jpg" title="">
                </div>

            </div><!--/row-->

            <div class="row">

                <div class="span12">
                    <h3>Museo dell'Identità - Nuoro</h3>
                    <p>L’edificio  dell’ex  “Mulino  Guiso  Gallisai”,  molto  caro  ai  nuoresi, rappresenta una preziosa testimonianza del passato legato alla crescita industriale della città, ed assume non solamente un valore architettonico, ma anche immateriale - simbolico, sia per la sua localizzazione, sia per ciò che l’immobile ha rappresentato per l’economia della città tra la fine dell’800 e i primi anni 50 del ‘900. In particolare deve evidenziarsi ciò che, all’epoca, il Mulino Guiso Gallisai ha rappresentato per la città di Nuoro, e certamente anche per la sua Provincia, in termini di lavoro, di sviluppo e di benessere economico in un’epoca, che va dalla fine dell’ottocento alla seconda metà del novecento, caratterizzata com’è noto da condizioni economiche di grande povertà (immortalate anche dalla Deledda nei suoi celebri romanzi), ascrivibili ad un tessuto economico fortemente arretrato basato quasi esclusivamente su attività agro pastorali, privo di alcuna struttura industriale, ed in cui era pressoché inesistente ogni forma di intervento pubblico che alleviasse la situazione di estremo disagio in cui vivevano le popolazioni</p>
                </div>

            </div>


            <hr>

            <footer>
                <p>&copy; Studio di architettura Marcon Architetti 2013</p>
            </footer>

        </div> <!-- /container -->

<?php include('_partials/footer.inc'); ?>