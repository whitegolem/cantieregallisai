
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>

		<script src="js/vendor/bootstrap.min.js"></script>

		<link href="js/vendor/swiper/dev/idangerous.swiper.css" media="all" type="text/css" rel="stylesheet">
		<script src="js/vendor/swiper/dev/idangerous.swiper.js"></script>

		<script src="js/main.js"></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-18430705-22', '95.110.200.241');
			ga('send', 'pageview');
		</script>
	</body>
</html>
