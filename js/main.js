/**
* imposta la classe del menu su active in base alla pagina visitata
*/
function setActiveMenu() {
	var $menuLink,
		page = (document.location.pathname).replace(/^\//i,'') || 'index.php';

	$menuLink = $('#menu').find('a[href="'+page+'"]');
	$menuLink.parent().addClass('active');
}

function enableSlider(selector) {
	var fixSlideHeight = function() {
			var screenHeight = $('.swiper-outer').height(),
				imgHeight = $('.swiper-slide').find('img').height();
				
			$('.swiper-outer, .swiper-slide').height(imgHeight);
		},
		$slider = $(selector);

	if($slider.length>0) {
			
		//Main Swiper
		var $slideImages = $('.swiper-slide').find('img'),
			swiper = new Swiper(selector, {
			pagination : '.my_pagination',
			loop:true,
			grabCursor: true
		});
		//Navigation arrows
		$('.arrow-left').click(function(e) {
			e.preventDefault()
			swiper.swipePrev()
		});
		$('.arrow-right').click(function(e) {
			e.preventDefault()
			swiper.swipeNext()
		});
		//Clickable pagination
		$('.my_pagination .swiper-pagination-switch').click(function(){
			swiper.swipeTo($(this).index())
		});

		$(window).on('resize load', function(e){
			fixSlideHeight(); //corregge bug chrome
		});
	}
	return $slider;
}

(function($){

	$(function(){
		setActiveMenu();
		enableSlider('.my_slider');
	});

})(jQuery);
